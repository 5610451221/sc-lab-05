package people;

public class Professor {
	private String name;
	private String id;
	
	public Professor(String name,String id){
		this.name = name;
		this.id = id;
	}

//get set method -----------------------------------------------------	
	//a	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	//a
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

}
