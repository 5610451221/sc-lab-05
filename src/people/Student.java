package people;

import java.util.ArrayList;

import library.Book;

public class Student {
	private String name;
	private String id;
	private String code;
	private String degree;
	private ArrayList<Book> bookList;
	
	public Student(String name,String id,String code,String degree){
		this.bookList = new ArrayList<Book>();
		this.name = name;
		this.id = id;
		this.code = code;
	}
	
//get set method -----------------------------------------------------	
	//a
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	//a
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
    //a
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	//a
	public String getDegree() {
		return degree;
	}
	
	public void setDegree(String degree) {
		this.degree = degree;
	}
	
	public void addBookToList(Book namebook){
		bookList.add(namebook);
		
	}
	
	public boolean checkBookInList(Book namebook){
		if(bookList.contains(namebook)){
			return true;
		}
		else{
			return false;
		}
	}
	
	public String removeBookInList(Book namebook){
		bookList.remove(namebook);
		return "Return Already";
	}

}
