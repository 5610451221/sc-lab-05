package library;

public class ReferenceBook {
	private String name;
	private String publish;
	
	public ReferenceBook(String name,String publish){
		this.name = name;
		this.publish = publish;
	}
	
	
//get set method -----------------------------------------------------	
	//a	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	//a
	public String getPublish() {
		return publish;
	}
	public void setPublish(String publish) {
		this.publish = publish;
	}
	
	
	
}
