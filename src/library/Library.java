package library;

import java.util.ArrayList;
import java.util.Collection;

import people.Student;

public class Library {
	private Book book;
	private ReferenceBook refBook;
	private ArrayList<Book> bookList = new ArrayList<Book>();
	private ArrayList<ReferenceBook> refList = new ArrayList<ReferenceBook>();
	private Student stu;
		
	
//Add ----------------------------------------------------------		
	public void addBook(Book book){
		bookList.add(book);
	}
	
	public ReferenceBook getRefBook() {
		return refBook;
	}


	public ArrayList<Book> getBookList() {
		return bookList;
	}


	public ArrayList<ReferenceBook> getRefList() {
		return refList;
	}


	public ArrayList<ReferenceBook> addRefBook(ReferenceBook refBook){
		refList.add(refBook);
		return refList;
	}
	

//DeleteBook -----------------------------------------------------
	public void deleteBook(Book book){
		bookList.remove(book);
	}
	
	public void deleteRefBook(ReferenceBook refBook){
		refList.remove(refBook);
	}
	
	
//Borrow ---------------------------------------------------------
	public boolean borrowRef(Student stu,ReferenceBook ref){
		return false;
	}
	
	
	
	public boolean borrowBook(Student stu,Book namebook){

		if(namebook.getCheckBook() == "available" && bookList.contains(namebook) ){
			stu.addBookToList(namebook);
			namebook.setCheckBook("unavailable");
			return true;
		}
		else{
			return false;
		}
		
		

	}
	
	
//Return ----------------------------------------------------------	
	public boolean returnBook(Student stu, Book namebook){
		if(stu.checkBookInList(namebook)){
			stu.removeBookInList(namebook);
			namebook.setCheckBook("available");
		}
		return true;
	}
	

//Count ------------------------------------------------------------	
	public ArrayList<Book> getBookCount() {
		return bookList;
	}
	public ArrayList<ReferenceBook> getRefCount() {
		return refList;
	}

	
	
	
	

}
