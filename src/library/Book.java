package library;

public class Book {
	private String name;
	private String publish;
	private String checkBook;
	
	public Book(String name,String publish){
		this.name = name;
		this.publish = publish;
		this.checkBook = "available";
	}
	
 
	
    public String getCheckBook() {
		return checkBook;
	}



	public void setCheckBook(String checkBook) {
		this.checkBook = checkBook;
	}



	//get set method -----------------------------------------------------	
	//a
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	//a
	public String getPublish() {
		return publish;
	}
	public void setPublish(String publish) {
		this.publish = publish;
	}


}
