package control;

import java.util.ArrayList;

import people.Authorities;
import people.Professor;
import people.Student;
import library.Book;
import library.Library;
import library.ReferenceBook;

public class LibraryManagement {
	
	public LibraryManagement() {
		Student stu1 = new Student("May","5610451221","D14","bachelor");
		Student stu2 = new Student("Biew","<3","D01","Doctor");
		Student stu3 = new Student("Ployphuk","3333","D14", "bachelor");
		
		
		Book book1 = new Book("Big JAVA","2012");
		Book book2 = new Book("PPL","2014");
		Book book3 = new Book("DeDee","2015");
		
		ReferenceBook refBook1 = new ReferenceBook("JAVA Ref","2014");
		ReferenceBook refBook2 = new ReferenceBook("DeDee Ref","2015");
		ReferenceBook refBook3 = new ReferenceBook("Bio Ref","2012");
		
		Library lib = new Library();
		lib.addBook(book1);
		lib.addBook(book2);
		lib.addBook(book3);
		lib.addRefBook(refBook1);
		lib.addRefBook(refBook2);
		lib.addRefBook(refBook3);
		
		printRefBook(lib.getRefCount());
		printBook(lib.getBookCount());
		
				
		System.out.println((lib.borrowBook(stu1, book1)));
		System.out.println((lib.returnBook(stu1, book1)));
		System.out.println((lib.borrowRef(stu1, refBook1)+"\n"));
		
		
		System.out.println(lib.borrowRef(stu2, refBook2));
		System.out.println((lib.borrowBook(stu2, book2)));
		System.out.println((lib.returnBook(stu2, book2) + "\n"));
		
		System.out.println((lib.borrowBook(stu3, book3)));
		System.out.println((lib.borrowRef(stu3, refBook3)));
		System.out.println((lib.returnBook(stu3, book3) + "\n"));
		
	}

	public static void main(String[] args) {
		new LibraryManagement();
		
		
//		System.out.println(lib.getBookCount());
//		
//		lib.borrowBook(stu,"PPL");
//		System.out.println(lib.getBookCount());
//		
//		lib.returnBook(stu,"PPL");
//		System.out.println(lib.getBookCount());
//		
//		lib.borrowBook(stu, "JAVA reference");
//		System.out.println(lib.getBookCount());
//		
		
		

	}
	
	public void printBook(ArrayList<Book> book){
		for (Book Book : book) {
			System.out.println(Book.getName() +" " + Book.getPublish());
			}
		}
	
	public void printRefBook(ArrayList<ReferenceBook> ref){
		for (ReferenceBook referenceBook : ref) {
			System.out.println(referenceBook.getName() +" " +referenceBook.getPublish());
		}

	}

}
